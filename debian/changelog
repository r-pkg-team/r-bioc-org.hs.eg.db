r-bioc-org.hs.eg.db (3.20.0-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 09:10:50 +0100

r-bioc-org.hs.eg.db (3.20.0-2) UNRELEASED; urgency=medium

  * Team upload.
  * d/control: Fix homepage URL. Closes: #1083247

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 12 Jan 2025 15:32:02 +0100

r-bioc-org.hs.eg.db (3.20.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Nov 2024 09:33:53 +0100

r-bioc-org.hs.eg.db (3.19.1-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 15:01:36 +0200

r-bioc-org.hs.eg.db (3.19.1-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 23 Jul 2024 17:36:14 +0200

r-bioc-org.hs.eg.db (3.19.1-1~0exp) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 18:54:38 +0200

r-bioc-org.hs.eg.db (3.18.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 04 Dec 2023 19:27:32 +0100

r-bioc-org.hs.eg.db (3.17.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 22 Jul 2023 21:50:50 +0200

r-bioc-org.hs.eg.db (3.16.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 23 Nov 2022 09:22:05 +0100

r-bioc-org.hs.eg.db (3.15.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 18 May 2022 11:27:34 +0200

r-bioc-org.hs.eg.db (3.14.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 29 Nov 2021 17:18:33 +0100

r-bioc-org.hs.eg.db (3.13.0-2) unstable; urgency=medium

  * Team upload.
  * Disable reprotest
  * Provide autopkgtest-pkg-r.conf to make sure RUnit will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Thu, 16 Sep 2021 09:36:57 +0200

r-bioc-org.hs.eg.db (3.13.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 16 Sep 2021 06:47:47 +0200

r-bioc-org.hs.eg.db (3.12.0-1) unstable; urgency=medium

  * Team upload.
  * Fix watch file
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 04 Nov 2020 18:32:16 +0100

r-bioc-org.hs.eg.db (3.11.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Wed, 03 Jun 2020 08:14:26 +0200

r-bioc-org.hs.eg.db (3.11.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 19 May 2020 12:11:34 +0200

r-bioc-org.hs.eg.db (3.10.0-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0
  * Trim trailing whitespace.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 03 Feb 2020 07:37:03 +0100

r-bioc-org.hs.eg.db (3.10.0-1) unstable; urgency=medium

  * Initial release (closes: #948906)

 -- Steffen Moeller <moeller@debian.org>  Tue, 14 Jan 2020 14:36:37 +0100
